import Vue from 'vue'
import Vuex from 'vuex'

export default{
	namespaced: true,
	state:{
		ordenes: [],
	},

	mutations:{
		ORDENES(state, data){
			state.ordenes = data
		},
	
	},
	actions:{
		guardar_datos_locales({commit}, payload){
			commit('ORDENES', payload);
		},
  },

	getters:{
		obtener_ordenes(state){
		  return state.ordenes
		},
	}
}