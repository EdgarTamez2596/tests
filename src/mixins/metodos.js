export default {
	computed:{
		tamanioPantalla () {
			return this.$vuetify.breakpoint.height -280
		},
	},

	filters:{
		currency(val, dec){
			 return '$' + parseFloat(val).toFixed(dec);
		},
	},
}