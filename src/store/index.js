import Vue from 'vue'
import Vuex from 'vuex'
import Orders from '@/modules/Orders';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    Orders
  }
})
