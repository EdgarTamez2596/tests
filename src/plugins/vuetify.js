import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#0096cb',
        secondary: '#000046',  
        error:'#D50000',
        info: '#009688',
        warning: '#FFC107'    
      },
    },
  },
});
