import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

import orders_list from '@/views/Orders/orders_list.vue'
import purchase_order_detail from '@/views/Orders/purchase_order_detail.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', name: 'orders_list', component: orders_list  },
  { path: '/purchase.order.detail', name: 'purchase_order_detail', component: purchase_order_detail  },
]

const router = new VueRouter({
  routes
})

export default router
